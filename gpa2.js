
// Jean Reno Information
let studentJean = {
    name: 'Jean',
    surname: 'Reno',
    age: '26',
    scores: {
        frontend: {
            javascript: 62,
            react: 57,
        },

        backend: {
            python: 88,
            java: 90
        }
    }

}

//Claude Monet Information
let studentClaude = {
    name: 'Claude',
    surname: 'Monet',
    age: 19,
    scores: {
        frontend: {
            javascript: 77,
            react: 52,
        },

        backend: {
            python: 92,
            java: 67
        }
    }
}

//Van Gogh Infomation
let studentVan = {
    name: 'Van',
    surname: 'Gogh',
    age: 21,
    scores: {
        frontend: {
            javascript: 51,
            react: 98
        },

        backend: {
            python: 65,
            java: 70
        }
    }
}

// Dam Square Information
let studentDam = {
    name: 'Dam',
    surname: 'Square',
    age: 36,
    scores: {
        frontend: {
            javascript: 82,
            react: 53
        },

        backend: {
            python: 80,
            java: 65
        }
    }
}

//GPA Information
let gpa = {
    '51-60': 0.5,
    '61-70': 1,
    '71-80': 2,
    '81-90': 3,
    '91-100': 4 
}

let credits = {
    javascript: 4,
    react: 7,
    python: 6,
    java: 3
}

let totalCredits = credits.javascript + 
                   credits.react + 
                   credits.python + 
                   credits.java;


// Total Scores:

let totalScoreJean = studentJean.scores.frontend.javascript +
                     studentJean.scores.frontend.react + 
                     studentJean.scores.backend.java+
                     studentJean.scores.backend.python;

let totalScoreClaude = studentClaude.scores.frontend.javascript + 
                       studentClaude.scores.frontend.react +
                       studentClaude.scores.backend.java + 
                       studentClaude.scores.backend.python;

let totalScoreVan = studentVan.scores.frontend.javascript + 
                    studentVan.scores.frontend.react + 
                    studentVan.scores.backend.java + 
                    studentVan.scores.backend.python;

let totalScoreDam = studentDam.scores.frontend.javascript + 
                    studentDam.scores.frontend.react + 
                    studentDam.scores.backend.java + 
                    studentDam.scores.backend.python;   


// Average scores:

let averageScoreJean = totalScoreJean / 4;
let averageScoreClaude = totalScoreClaude / 4;
let averageScoreVan = totalScoreVan / 4;
let averageScoreDam = totalScoreDam / 4;

// GPA calculation
let jeanGPA = (credits.javascript * gpa['61-70'] + 
              credits.react * gpa['51-60'] + 
              credits.python * gpa['81-90'] + 
              credits.java * gpa['81-90']) / totalCredits;

let claudeGPA = (credits.javascript * gpa['71-80'] + 
                credits.react * gpa['51-60'] + 
                credits.python * gpa['91-100'] + 
                credits.java * gpa['61-70']) / totalCredits;

let vanGPA =    (credits.javascript * gpa['51-60'] + 
                credits.react * gpa['91-100'] + 
                credits.python * gpa['61-70'] + 
                credits.java * gpa['61-70']) / totalCredits;

let damGPA =    (credits.javascript * gpa['81-90'] + 
                credits.react * gpa['51-60'] + 
                credits.python * gpa['71-80'] + 
                credits.java * gpa['61-70']) / totalCredits;

// Print information about total score, avarage score and GPA

let infoJean = `Student name is ${studentJean.name}, total score: ${totalScoreJean} average score: ${averageScoreJean}, GPA: ${jeanGPA}`;
let infoClaude = `Student name is ${studentClaude.name}, total score: ${totalScoreClaude} average score: ${averageScoreClaude}, GPA: ${claudeGPA}`;
let infoVan = `Student name is ${studentVan.name}, total score: ${totalScoreVan} average score: ${averageScoreVan}, GPA: ${vanGPA}`;
let infoDam  = `Student name is ${studentDam.name}, total score: ${totalScoreDam} average score: ${averageScoreDam}, GPA: ${damGPA}`;

console.log(infoJean);
console.log(infoClaude);
console.log(infoVan);
console.log(infoDam);
console.log('');

//Task 3 

let averageTotal = (totalScoreJean + totalScoreClaude + totalScoreVan + totalScoreDam) / 16;

let isRedDiplomaJean = averageScoreJean >= averageTotal ? 'Red Diploma' : 'Vrag Naroda';
let isRedDiplomaClaude = averageScoreClaude >= averageTotal ? 'Red Diploma' : 'Vrag Naroda';
let isRedDiplomaVan = averageScoreVan >= averageTotal ? 'Red Diploma' : 'Vrag Naroda';
let isRedDiplomaDam = averageScoreDam >= averageTotal ? 'Red Diploma' : 'Vrag Naroda';

console.log(`Student ${studentJean.name} status is ${isRedDiplomaJean}`);
console.log(`Student ${studentClaude.name} status is ${isRedDiplomaClaude}`);
console.log(`Student ${studentVan.name} status is ${isRedDiplomaVan}`);
console.log(`Student ${studentDam.name} status is ${isRedDiplomaDam}`);
console.log('');

//Task 4 
// Check !!!!!!!!!!!!!!
let isBest = jeanGPA ;

isBest = claudeGPA > isBest ? claudeGPA  : isBest;
isBest = vanGPA > isBest ? jeanGPA  : isBest;
isBest = damGPA > isBest ? damGPA  : isBest;

console.log(isBest);


//Task 5

let bestAverage ;

if(studentClaude.age > 21 &&
   claudeGPA > jeanGPA &&
   claudeGPA > vanGPA &&
   claudeGPA > damGPA) {
      console.log( 'Claude is the best above 21' )
   } else if (studentJean.age > 21 &&
              jeanGPA > claudeGPA &&
              jeanGPA > vanGPA &&
              jeanGPA > damGPA) {
                console.log( 'Jean is the best above 21' )   
     } else if (studentVan.age > 21 &&
                vanGPA > jeanGPA &&
                vanGPA > claudeGPA &&
                vanGPA > damGPA) {
                console.log( 'Van is the best above 21' )
     } else if(studentDam.age > 21 &&
                  damGPA > jeanGPA &&
                  damGPA > claudeGPA &&
                  damGPA > vanGPA) {
                 console.log( 'Dam is the best above 21' )
                                   }


//Task 6 Best in frontend

let studentJeanFrontAverage = (studentJean.scores.frontend.javascript + studentJean.scores.frontend.react ) / 2;
let studentClaudeFrontAverage = (studentClaude.scores.frontend.javascript + studentClaude.scores.frontend.react ) / 2;
let studentVanFrontAverage = (studentVan.scores.frontend.javascript + studentVan.scores.frontend.react ) / 2;
let studentDamFrontAverage = (studentDam.scores.frontend.javascript + studentDam.scores.frontend.react ) / 2;

if(studentJeanFrontAverage > studentClaudeFrontAverage && 
    studentJeanFrontAverage > studentVanFrontAverage &&
    studentJeanFrontAverage > studentDamFrontAverage) {
        console.log('Student Jean in the best in frontend')
    } else if(studentClaudeFrontAverage > studentJeanFrontAverage &&
              studentClaudeFrontAverage > studentVanFrontAverage &&
              studentClaudeFrontAverage > studentDamFrontAverage){
              console.log('Student Claude in the best in frontend')  
     } else if(studentVanFrontAverage > studentJeanFrontAverage &&
               studentVanFrontAverage > studentClaudeFrontAverage &&
               studentVanFrontAverage > studentDamFrontAverage){
                console.log('Student Van in the best in frontend')
     } else if(studentDamFrontAverage > studentJeanFrontAverage &&
               studentDamFrontAverage > studentJeanFrontAverage &&
               studentDamFrontAverage > studentVanFrontAverage) {
                console.log('Student Dam in the best in frontend')
               }


